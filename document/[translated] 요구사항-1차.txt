개요 - Khái quát : 
	전자 설문 조사- Khảo sát điện tử ( Electronic Surveys)
	전자 설문을 실시하고 그 결과를 분석하여 보고서를 생성한다. - Cho người dùng làm khảo sát sau đó nhận kết quả khảo sát ấy về và tiến hành tạo bảng báo cáo kết quả. 
	전자 설문은 공개, 비공개로 나뉜다.- Khảo sát được chia làm : public và private.
		공개 - 누구나 설문에 응답할 수 있다. Public: ai cũng làm được khảo khát
		비공개 - 관리자에 의해 부여된 코드를 확인한 사람들만 응답할 수 있다.Private: dựa nào admin phân quyền - người được admin cho phép mới có thể làm khảo sát
설문의 구성 - Cấu tạo của khảo sát 
	단락 : 연관성 있는 문항들의 집합- Đoạn văn: tập hợp những câu hỏi có tính liên quan với nhau
	문항 : 사용자에게 보여지는 질문 - Câu hỏi: Những câu hỏi dành cho người dùng  
	응답 : 각 문항에 대한 사용자의 입력 - Câu trả lời: nội dung nhập vào của người dùng cho mỗi câu hỏi 

응답 방법의 종류 - Các dạng của cách thức trả lời 
	(1) 보기 : 문항에 대한 응답으로 선택할 수 있는 것 - Đọc : Chọn đáp án phù hợp với câu hỏi
	(2) 정수 : 문항에 대한 응답으로 정수 입력 - Số nguyên: Nhập số nguyên ứng với đáp án của câu hỏi

관리자- Admin 
	설문 관리- Quản lí khảo sát
	응답 분석 - Phân tích kết quả ks
	응답자 관리- Quản lí người dùng 

고객 - Client
	설문 조사를 의뢰하는 기업 또는 개인 - Cá nhân hoặc tổ chức người ủy quyền thực hiện khảo sát cho họ
	설문 조사 현황과 응답 분석 자료를 볼 수 있다. - Họ có thể xem được trạng thái khảo sát và tài liệu phân tích kết quả khảo khát ( kq phân tích câu trả lời)
		설문의 문항, 응답 분석을 직접할 수 없다. - Không thể tự mình phân tích kết quả câu trả lời , câu hỏi của khảo sát. 
	대상자 명단을 전자 또는 문서로 등록할 수 있다. - Danh sách các ứng viên có thể được đăng ký dưới dạng điện tử hoặc bằng văn bản. 
		응답자의 코드를 정할 수 없다. Không được chỉ định mã code người trả lời 

운영 - Vận hành 
	Web 기반 (반응형) - dựa trên Web( responsive) 
	Mobile App 은 차세대 개발 시 진행- Mobile App: sẽ tiến hành sau