package com.theplayer.appendix.reponsitory.impl;

import java.util.List;

import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.theplayer.appendix.entity.Organizations;
import com.theplayer.appendix.reponsitory.OrganizationRepository;

@Repository
public class OrganizationRepositoryImpl implements OrganizationRepository{

	@Autowired
	SqlSessionTemplate session;
	
	@Override
	public void insert(Organizations vo) {
		// TODO Auto-generated method stub
		session.insert("Organizations.insert", vo);
	}

	@Override
	public void delete(Organizations vo) {
		// TODO Auto-generated method stub
		session.delete("Organizations.delete", vo);
	}

	@Override
	public void update(Organizations vo) {
		// TODO Auto-generated method stub
		session.update("Organizations.update", vo);
	}

	@Override
	public List<Organizations> list() {
		// TODO Auto-generated method stub
		return session.selectList("Organizations.selectList");
	}

	@Override
	public Organizations get(Organizations vo) {
		// TODO Auto-generated method stub
		return (Organizations)session.selectOne("Organizations.selectOne", vo);
	}

}
