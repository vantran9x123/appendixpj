package com.theplayer.appendix.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.theplayer.appendix.entity.Organizations;
import com.theplayer.appendix.reponsitory.impl.OrganizationRepositoryImpl;
import com.theplayer.appendix.service.OrganizationService;

@Service
public class OrganizationServiceImpl implements OrganizationService{

	@Autowired
	private OrganizationRepositoryImpl organizationRepoImpl;
	
	@Override
	public void insert(Organizations vo) {
		// TODO Auto-generated method stub
		organizationRepoImpl.insert(vo);
	}

	@Override
	public void delete (Organizations vo) {
		// TODO Auto-generated method stub
		organizationRepoImpl.delete(vo);
	}

	@Override
	public void update(Organizations vo) {
		// TODO Auto-generated method stub
		organizationRepoImpl.update(vo);
	}

	@Override
	public List<Organizations> list() {
		// TODO Auto-generated method stub
		return organizationRepoImpl.list();
	}

	@Override
	public Organizations get(Organizations vo) {
		// TODO Auto-generated method stub
		return (Organizations)organizationRepoImpl.get(vo);
	}

}
