package com.theplayer.appendix.controller;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.theplayer.appendix.entity.Organizations;
import com.theplayer.appendix.service.impl.OrganizationServiceImpl;


@Controller
@RequestMapping("/admin")
public class AdminOrganizationController {
	
	private static final Logger logger = LoggerFactory.getLogger(AdminOrganizationController.class);
	
	@Autowired
	OrganizationServiceImpl orgServiceImpl;
	
	@RequestMapping(value = {"","/"}, method = RequestMethod.GET)
	public String ahomeSurvey(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		
		model.addAttribute("serverTime", formattedDate );
		
		System.out.println("------------- Survey -------------");
		
		return "admin/adminHome";
	}
	
	@RequestMapping(value = "/organization", method = RequestMethod.GET)
	public String listOrganization(Locale locale, Model model) {
		logger.info("List organization {}.", locale);
		
		List<Organizations> list = orgServiceImpl.list();
		model.addAttribute("listOrganization", list);
		
		System.out.println(" Survey organization : " + list);
		
		return "/admin/organization/organization";
	}
	
	@RequestMapping(value = "/organization/new", method = RequestMethod.GET)
	public String newOrganization(Locale locale, Model model) {
		logger.info("New organization {}.", locale);
		
		return "/admin/organization/newOrganization";
	}
	
	@RequestMapping(value = "/organization/new", method = RequestMethod.POST)
	public String saveOrganization(Locale locale, Organizations organizations, Model model) {
		logger.info("Save organization {}.", locale);
		
		orgServiceImpl.insert(organizations);
		
		System.out.println("------------- Save organization -------------");
		
		return "redirect:/admin/organization/organization";
	}
	
	@RequestMapping(value = "/organization/edit", method = RequestMethod.GET)
	public String editOrganization(@RequestParam("id")int id, Locale locale, Model model) {
		logger.info("Edit organization {}.", locale);
		
		Organizations vo = new Organizations();
		vo.setId(id);
		Organizations org = orgServiceImpl.get(vo);
		model.addAttribute("organization", org);
		
		return "/admin/organization/editOrganization";
	}
	
	@RequestMapping(value = "/organization/edit", method = RequestMethod.POST)
	public String updateOrganization(Locale locale, Organizations organizations, Model model) {
		logger.info("Update organization {}.", locale);
		
		orgServiceImpl.update(organizations);
		
		return "redirect:/admin/organization";
	}
	
}