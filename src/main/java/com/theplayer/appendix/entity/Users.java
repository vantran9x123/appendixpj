package com.theplayer.appendix.entity;

import java.util.Date;

public class Users {
	private int id;
	private String username;
	private String password;
	private String email;
	private boolean admin;
	private Date inviteDateTime;
	private Date lastLogin;
	private Users inviter;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public boolean isAdmin() {
		return admin;
	}
	public void setAdmin(boolean admin) {
		this.admin = admin;
	}
	public Date getInviteDateTime() {
		return inviteDateTime;
	}
	public void setInviteDateTime(Date inviteDateTime) {
		this.inviteDateTime = inviteDateTime;
	}
	public Date getLastLogin() {
		return lastLogin;
	}
	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}
	public Users getInviter() {
		return inviter;
	}
	public void setInviter(Users inviter) {
		this.inviter = inviter;
	}
}
