package com.theplayer.appendix.entity;

public class Questions {
	private int id;
	private Questions parent;
	private SurveySections surveySections;
	private InputType inputType;
	private String name;
	private String subtext;
	private boolean questionRequiredYN;
	private boolean answerRequiredYN;
	private OptionGroups optionGroups;
	private boolean allowMultipleOptionAnswerYN;
	private Questions dequendentQuestion; 
	private QuestionOptions dequendentQuestionOption;
	private Answers dependentAnswer;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Questions getParent() {
		return parent;
	}
	public void setParent(Questions parent) {
		this.parent = parent;
	}
	public SurveySections getSurveySections() {
		return surveySections;
	}
	public void setSurveySections(SurveySections surveySections) {
		this.surveySections = surveySections;
	}
	public InputType getInputType() {
		return inputType;
	}
	public void setInputType(InputType inputType) {
		this.inputType = inputType;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSubtext() {
		return subtext;
	}
	public void setSubtext(String subtext) {
		this.subtext = subtext;
	}
	public boolean isQuestionRequiredYN() {
		return questionRequiredYN;
	}
	public void setQuestionRequiredYN(boolean questionRequiredYN) {
		this.questionRequiredYN = questionRequiredYN;
	}
	public boolean isAnswerRequiredYN() {
		return answerRequiredYN;
	}
	public void setAnswerRequiredYN(boolean answerRequiredYN) {
		this.answerRequiredYN = answerRequiredYN;
	}
	public OptionGroups getOptionGroups() {
		return optionGroups;
	}
	public void setOptionGroups(OptionGroups optionGroups) {
		this.optionGroups = optionGroups;
	}
	public boolean isAllowMultipleOptionAnswerYN() {
		return allowMultipleOptionAnswerYN;
	}
	public void setAllowMultipleOptionAnswerYN(boolean allowMultipleOptionAnswerYN) {
		this.allowMultipleOptionAnswerYN = allowMultipleOptionAnswerYN;
	}
	public Questions getDequendentQuestion() {
		return dequendentQuestion;
	}
	public void setDequendentQuestion(Questions dequendentQuestion) {
		this.dequendentQuestion = dequendentQuestion;
	}
	public QuestionOptions getDequendentQuestionOption() {
		return dequendentQuestionOption;
	}
	public void setDequendentQuestionOption(QuestionOptions dequendentQuestionOption) {
		this.dequendentQuestionOption = dequendentQuestionOption;
	}
	public Answers getDependentAnswer() {
		return dependentAnswer;
	}
	public void setDependentAnswer(Answers dependentAnswer) {
		this.dependentAnswer = dependentAnswer;
	}
	
	
}
