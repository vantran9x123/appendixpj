package com.theplayer.appendix.entity;

import java.util.Date;

public class UserSurveySections {
	private int id;
	private Users users;
	private SurveySections surveySections;
	private Date completedOn;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Users getUsers() {
		return users;
	}
	public void setUsers(Users users) {
		this.users = users;
	}
	public SurveySections getSurveySections() {
		return surveySections;
	}
	public void setSurveySections(SurveySections surveySections) {
		this.surveySections = surveySections;
	}
	public Date getCompletedOn() {
		return completedOn;
	}
	public void setCompletedOn(Date completedOn) {
		this.completedOn = completedOn;
	}
}
