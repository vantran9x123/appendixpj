package com.theplayer.appendix.entity;

public class SurveyComments {
	private int id;
	private SurveyHeaders surveyHeaders;
	private Users user;
	private String comments;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public SurveyHeaders getSurveyHeaders() {
		return surveyHeaders;
	}
	public void setSurveyHeaders(SurveyHeaders surveyHeaders) {
		this.surveyHeaders = surveyHeaders;
	}
	public Users getUser() {
		return user;
	}
	public void setUser(Users user) {
		this.user = user;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	
}
