package com.theplayer.appendix.entity;

public class SurveySections {
	private int id;
	private SurveyHeaders surveyHeaders;
	private String name;
	private String title;
	private String subHeading;
	private boolean requiredYN;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public SurveyHeaders getSurveyHeaders() {
		return surveyHeaders;
	}
	public void setSurveyHeaders(SurveyHeaders surveyHeaders) {
		this.surveyHeaders = surveyHeaders;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSubHeading() {
		return subHeading;
	}
	public void setSubHeading(String subHeading) {
		this.subHeading = subHeading;
	}
	public boolean isRequiredYN() {
		return requiredYN;
	}
	public void setRequiredYN(boolean requiredYN) {
		this.requiredYN = requiredYN;
	}
	
}
