package com.theplayer.appendix.entity;

public class QuestionOptions {
	private int id;
	private Questions questions;
	private OptionChoices optionChoices;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Questions getQuestions() {
		return questions;
	}
	public void setQuestions(Questions questions) {
		this.questions = questions;
	}
	public OptionChoices getOptionChoices() {
		return optionChoices;
	}
	public void setOptionChoices(OptionChoices optionChoices) {
		this.optionChoices = optionChoices;
	}
	
}
