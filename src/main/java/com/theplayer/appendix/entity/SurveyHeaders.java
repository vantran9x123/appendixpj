package com.theplayer.appendix.entity;

public class SurveyHeaders {
	private int id;
	private Organizations organizations;
	private String name;
	private String instructions;
	private String orderHeaderInfo;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Organizations getOrganizations() {
		return organizations;
	}
	public void setOrganizations(Organizations organizations) {
		this.organizations = organizations;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getInstructions() {
		return instructions;
	}
	public void setInstructions(String instructions) {
		this.instructions = instructions;
	}
	public String getOrderHeaderInfo() {
		return orderHeaderInfo;
	}
	public void setOrderHeaderInfo(String orderHeaderInfo) {
		this.orderHeaderInfo = orderHeaderInfo;
	}
	
}
