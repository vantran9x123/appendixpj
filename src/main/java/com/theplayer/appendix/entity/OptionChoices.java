package com.theplayer.appendix.entity;

public class OptionChoices {
	private int id;
	private OptionGroups optionGroups;
	private String name;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public OptionGroups getOptionGroups() {
		return optionGroups;
	}
	public void setOptionGroups(OptionGroups optionGroups) {
		this.optionGroups = optionGroups;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
