package com.theplayer.appendix.entity;

public class Answers {
	private int id;
	private Users users;
	private QuestionOptions questionOptions;
	private int answerNumeric;
	private String answerText;
	private boolean answerYN;
	private UnitOfMeasures unitOfMeasures;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Users getUsers() {
		return users;
	}
	public void setUsers(Users users) {
		this.users = users;
	}
	public QuestionOptions getQuestionOptions() {
		return questionOptions;
	}
	public void setQuestionOptions(QuestionOptions questionOptions) {
		this.questionOptions = questionOptions;
	}
	public int getAnswerNumeric() {
		return answerNumeric;
	}
	public void setAnswerNumeric(int answerNumeric) {
		this.answerNumeric = answerNumeric;
	}
	public String getAnswerText() {
		return answerText;
	}
	public void setAnswerText(String answerText) {
		this.answerText = answerText;
	}
	public boolean isAnswerYN() {
		return answerYN;
	}
	public void setAnswerYN(boolean answerYN) {
		this.answerYN = answerYN;
	}
	public UnitOfMeasures getUnitOfMeasures() {
		return unitOfMeasures;
	}
	public void setUnitOfMeasures(UnitOfMeasures unitOfMeasures) {
		this.unitOfMeasures = unitOfMeasures;
	}
}
